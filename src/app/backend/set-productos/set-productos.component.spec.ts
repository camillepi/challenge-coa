import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetProductosComponent } from './set-productos.component';

describe('SetProductosComponent', () => {
  let component: SetProductosComponent;
  let fixture: ComponentFixture<SetProductosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetProductosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetProductosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
