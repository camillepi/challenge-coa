import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarritoitemComponent } from './carritoitem.component';

describe('CarritoitemComponent', () => {
  let component: CarritoitemComponent;
  let fixture: ComponentFixture<CarritoitemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarritoitemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarritoitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
