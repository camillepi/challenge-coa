import { Component, OnInit, Input } from '@angular/core';
import { Producto } from 'src/app/models/producto';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css'],
})
export class ProductoComponent implements OnInit {
  @Input()
  productItem!: Producto;
  constructor() {}

  ngOnInit(): void {}
}
