import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductosuiComponent } from './productosui.component';

describe('ProductosuiComponent', () => {
  let component: ProductosuiComponent;
  let fixture: ComponentFixture<ProductosuiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductosuiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductosuiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
