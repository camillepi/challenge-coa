import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'src/app/servicios/producto.service';
import { Producto } from 'src/app/models/producto';

@Component({
  selector: 'app-productosui',
  templateUrl: './productosui.component.html',
  styleUrls: ['./productosui.component.css'],
})
export class ProductosuiComponent implements OnInit {
  productList: Producto[] = [];
  constructor(private productService: ProductoService) {}

  ngOnInit(): void {
    this.productList = this.productService.getProducts();
  }
}
