import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private xauth: AngularFireAuth) {}

  // GoogleAuth() {
  //   return this.AutenticarProveedor(new auth.GoogleAuthProvider());
  // }

  // AutenticarProveedor(proveedor) {
  //   this.xauth.signInWithPopup(proveedor).then(() => {
  //   });
  // }
}
