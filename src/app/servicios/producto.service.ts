import { Injectable } from '@angular/core';
import { Producto } from '../models/producto';

@Injectable({
  providedIn: 'root',
})
export class ProductoService {
  products: Producto[] = [
    new Producto(
      0,
      'Item 1',
      'A description of the first item',
      500,
      'https://via.placeholder.com/200x150'
    ),
    new Producto(
      1,
      'Item 2',
      'A description of the second item',
      600,
      'https://via.placeholder.com/200x150'
    ),
    new Producto(
      2,
      'Item 3',
      'A description of the third item',
      700,
      'https://via.placeholder.com/200x150'
    ),
    new Producto(
      3,
      'Item 4',
      'A description of the fourth item',
      250,
      'https://via.placeholder.com/200x150'
    ),
    new Producto(
      4,
      'Item 5',
      'A description of the fifth item',
      120,
      'https://via.placeholder.com/200x150'
    ),
  ];
  constructor() {}

  getProducts(): Producto[] {
    return this.products;
  }
}
