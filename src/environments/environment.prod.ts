export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: 'AIzaSyCAPscjxMnN5nVdlEXynnWu9IFYtxrasFM',
    authDomain: 'challenge-coa.firebaseapp.com',
    projectId: 'challenge-coa',
    storageBucket: 'challenge-coa.appspot.com',
    messagingSenderId: '525970110995',
    appId: '1:525970110995:web:a5c66d593db0db53aa1de6',
    measurementId: 'G-3LVJEBRE9G',
  },
};
