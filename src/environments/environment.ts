// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCAPscjxMnN5nVdlEXynnWu9IFYtxrasFM',
    authDomain: 'challenge-coa.firebaseapp.com',
    projectId: 'challenge-coa',
    storageBucket: 'challenge-coa.appspot.com',
    messagingSenderId: '525970110995',
    appId: '1:525970110995:web:a5c66d593db0db53aa1de6',
    measurementId: 'G-3LVJEBRE9G',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
